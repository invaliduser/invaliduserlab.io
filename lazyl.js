window.addEventListener('load', (event) => {
    var lazyloadImages;

    if ("IntersectionObserver" in window) {
        lazyloadImages = document.querySelectorAll(".lazy");
        var imageObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    var image = entry.target;
                    image.classList.remove("lazy");
                    imageObserver.unobserve(image);
                }
            });
        });

        lazyloadImages.forEach(function(image) {
            imageObserver.observe(image);
        });
    } else {
        var lazyloadThrottleTimeout;
        lazyloadImages = document.querySelectorAll(".lazy");

        function lazyload() {
            if (lazyloadThrottleTimeout) {
                clearTimeout(lazyloadThrottleTimeout);
            }

            lazyloadThrottleTimeout = setTimeout(function() {
                var scrollTop = window.pageYOffset;
                lazyloadImages.forEach(function(img) {
                    if (img.offsetTop < (window.innerHeight + scrollTop)) {
                        img.src = img.dataset.src;
                        img.classList.remove('lazy');
                    }
                });
                if (lazyloadImages.length == 0) {
                    document.removeEventListener("scroll", lazyload);
                    window.removeEventListener("resize", lazyload);
                    window.removeEventListener("orientationChange", lazyload);
                }
            }, 20);
        }
        $(document).on("click", lazyload);
        $("body").on("DOMMouseScroll mousewheel scroll touchend", lazyload);
        window.addEventListener("scroll", lazyload);
        window.addEventListener("resize", lazyload);
        window.addEventListener("orientationChange", lazyload);
    };

    var lazyloadImagesb = document.querySelectorAll("img.lazy");
    var lazyloadThrottleTimeoutb;

    function lazyloadb() {
        if (lazyloadThrottleTimeoutb) {
            clearTimeout(lazyloadThrottleTimeoutb);
        }

        lazyloadThrottleTimeoutb = setTimeout(function() {
            var scrollTop = window.pageYOffset;
            lazyloadImagesb.forEach(function(img) {
                if (img.offsetTop < (window.innerHeight + scrollTop)) {
                    img.src = img.dataset.src;
                    img.classList.remove('lazy');
                }
            });
            if (lazyloadImages.length == 0) {
                document.removeEventListener("scroll", lazyloadb);
                window.removeEventListener("resize", lazyloadb);
                window.removeEventListener("orientationChange", lazyloadb);
            }
        }, 20);
    }
    $(document).on("click", lazyloadb);
    $("body").on("DOMMouseScroll mousewheel scroll touchend", lazyloadb);
    document.addEventListener("scroll", lazyloadb);
    window.addEventListener("resize", lazyloadb);
    window.addEventListener("orientationChange", lazyloadb);
})
