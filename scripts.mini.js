function getSearchParams() {
    if (location.search) {
        const values = location.search.replace('?', '').split('&').reduce((res, item) => {
            const data = item.trim().split('=');
            return {...res,
                [data[0]]: data[1]
            };
        }, {});

        return values;
    } else {
        return {};
    };
};
